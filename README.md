# Search API Elasticsearch Client 

This module provides a [Search API](https://www.drupal.org/project/search_api)
Backend for [ElasticSearch](https://www.elastic.co//).

This module uses the official [ElasticSearch PHP Client](https://github.com/elastic/elasticsearch-php).

It is therefore compatible with any version of ElasticSearch >= 8.

**Important:**

To give you the freedom to choose the ElasticSearch version on your own, the ES PHP Client is not a composer dependancy of this module. Based on your installed ES version, you have to install the related ES PHP Client using composer:

`composer require elasticsearch/elasticsearch ^8.11`

### Features

- Search API integration for indexing, field mapping, views etc.
- Facets
- More Like This
- Connector plugin type for external connector extensions
- GeoData support
- NGram analyzer support

### Credits

This module heavily based on the work of the [Search Api Opensearch](https://www.drupal.org/project/search_api_opensearch) module.
