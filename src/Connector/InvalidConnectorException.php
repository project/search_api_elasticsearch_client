<?php

namespace Drupal\search_api_elasticsearch_client\Connector;

/**
 * Exception for invalid connector plugins.
 */
class InvalidConnectorException extends \RuntimeException {
}
