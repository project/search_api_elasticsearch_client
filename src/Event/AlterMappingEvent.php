<?php

namespace Drupal\search_api_elasticsearch_client\Event;

use Drupal\Component\EventDispatcher\Event;

/**
 * Event triggered when index mapping is completed.
 */
class AlterMappingEvent extends Event {

  /**
   * Creates a new event.
   *
   * @param array $params
   *   The mapping params.
   * @param array $properties
   *   The properties.
   */
  public function __construct(
    protected array $params,
    protected array $properties,
  ) {
  }

  /**
   * Gets the params.
   *
   * @return array
   *   The params.
   */
  public function getParameters(): array {
    return $this->params;
  }

  /**
   * Gets the properties.
   *
   * @return array
   *   The param.
   */
  public function getProperties(): array {
    return $this->properties;
  }

  /**
   * Sets the properties.
   *
   * @param array $properties
   *   The param.
   *
   * @return $this
   *   The current object.
   */
  public function setProperties(array $properties): AlterMappingEvent {
    $this->properties = $properties;
    return $this;
  }

}
