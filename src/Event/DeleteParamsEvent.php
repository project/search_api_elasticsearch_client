<?php

namespace Drupal\search_api_elasticsearch_client\Event;

/**
 * Event triggered when delete params are built.
 */
class DeleteParamsEvent extends BaseParamsEvent {
}
