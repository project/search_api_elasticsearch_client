<?php

namespace Drupal\search_api_elasticsearch_client\Event;

/**
 * Event triggered when index params are built.
 */
class IndexParamsEvent extends BaseParamsEvent {
}
