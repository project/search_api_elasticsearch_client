<?php

namespace Drupal\search_api_elasticsearch_client\Event;

/**
 * Event triggered when search params are built.
 */
class QueryParamsEvent extends BaseParamsEvent {
}
