<?php

declare(strict_types=1);

namespace Drupal\search_api_elasticsearch_client\Plugin\search_api\data_type;

use Drupal\search_api\Plugin\search_api\data_type\TextDataType;

/**
 * Defines a class for the completion field type in Elasticsearch.
 *
 * @SearchApiDataType(
 *   id = "search_api_elasticsearch_client_completion",
 *   label = @Translation("Elasticsearch: Completion"),
 *   description = @Translation("A navigational feature to guide users to relevant results as they are typing."),
 *   fallback_type = "text",
 * )
 */
final class CompletionDataType extends TextDataType {

}
