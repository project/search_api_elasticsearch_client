<?php

namespace Drupal\search_api_elasticsearch_client\Plugin\search_api\data_type;

use Drupal\search_api\DataType\DataTypePluginBase;

/**
 * Provides a date range data type.
 *
 * @SearchApiDataType(
 *   id = "elasticsearch_date_range",
 *   label = @Translation("Elasticsearch Date Range"),
 *   description = @Translation("Date field that contains date ranges."),
 *   default = TRUE,
 * )
 */
class ElasticsearchDateRangeDataType extends DataTypePluginBase {

}
