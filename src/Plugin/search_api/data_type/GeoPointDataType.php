<?php

namespace Drupal\search_api_elasticsearch_client\Plugin\search_api\data_type;

use Drupal\geofield\GeoPHP\GeoPHPInterface;
use Drupal\search_api\DataType\DataTypePluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a string data type.
 *
 * @SearchApiDataType(
 *   id = "geo_point",
 *   label = @Translation("Geo Point"),
 *   description = @Translation("The geo_point field type."),
 *   default = "true"
 * )
 */
class GeoPointDataType extends DataTypePluginBase {

  /**
   * The geoPhpWrapper service.
   */
  protected GeoPHPInterface $geoPhpWrapper;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $plugin = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $plugin->setGeoPhpWrapper($container->get('geofield.geophp'));

    return $plugin;
  }

  /**
   * Retrieves the geoPhpWrapper service.
   *
   * @return \Drupal\geofield\GeoPHP\GeoPHPInterface
   *   The geoPhpWrapper service.
   */
  public function getGeoPhpWrapper(): GeoPHPInterface {
    /* @phpstan-ignore-next-line */
    return $this->geoPhpWrapper ?: \Drupal::service('geofield.geophp');
  }

  /**
   * Sets the geoPhpWrapper service.
   *
   * @param \Drupal\geofield\GeoPHP\GeoPHPInterface $geoPhpWrapper
   *   The geoPhpWrapper service.
   *
   * @return $this
   */
  public function setGeoPhpWrapper(GeoPHPInterface $geoPhpWrapper) {
    $this->geoPhpWrapper = $geoPhpWrapper;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getValue($value) {
    if (is_string($value)) {
      $geom = $this->getGeoPhpWrapper()->load($value);
      // Phpstan cannot resolve the geometry class as it is loaded with
      // a class map and include calls rather than following PSR standards.
      // @see geoPHP.inc
      // @phpstan-ignore-next-line
      if ($geom instanceof \Geometry) {
        return [
          // @phpstan-ignore-next-line
          'lat' => $geom->y(),
          // @phpstan-ignore-next-line
          'lon' => $geom->x(),
        ];
      }
    }
    return $value;
  }

}
