<?php

namespace Drupal\search_api_elasticsearch_client\Plugin\search_api\data_type;

use Drupal\search_api\DataType\DataTypePluginBase;

/**
 * Provides a string data type.
 *
 * @SearchApiDataType(
 *   id = "search_api_elasticsearch_client_object",
 *   label = @Translation("Object"),
 *   description = @Translation("Structured Object support"),
 *   default = "true"
 * )
 */
class ObjectDataType extends DataTypePluginBase {
}
